import time

import discord
import pygame
from requests import Session
import json
from pygame.locals import *
from _thread import *
from discord.ext import tasks, commands
from PIL import Image, ImageDraw
import os

URL = "https://api3.binance.com/api/v3/ticker/price?symbol="
WINDOWSIZE = (500, 500)
DISTANCE = 50
NBR = 50
SIZEFONT = 24
IDBOT = 0
IDMAINCHANNEL = 860224631311171654
IDCHANNELHOURS = 860224631311171654
IDCHANNELDAYS = 860224631311171654
ADMINROLEID = 859455686647480321
STARTWITH = "!"
TOKEN = "ODYwMjIyMjgwMDk2ODA5MDEw.YN4GbQ.kHozvmMgpvcWW5CM9o05pCuXbhs"
ACTIVITE = "le cour des cryptomonnaies"

#partie affichage
def getPriceBTCToEUR():
    session = Session()
    reponse = session.get(URL + "BTCEUR")
    data = json.loads(reponse.text)
    return data["price"]


def drawText(fenetre, font, text, color, x, y, center=False):
    text = font.render(text, True, color)
    if center:
        text_rect = text.get_rect(center=(x, y))
        fenetre.blit(text, text_rect)
    else:
        fenetre.blit(text, (x, y))


def getColorBetweenPrice(price1, price2):
    if price1 > price2:
        res = (255, 0, 0)
    elif price1 < price2:
        res = (0, 255, 0)
    else:
        res = (0, 0, 255)

    return res


def mapValue(value, minValue, maxValue, minBound, maxBound):
    newValue = normalizeValue(value, minValue, maxValue)
    denum = maxBound - minBound
    r = -minBound / denum
    s = 1 / denum
    result = (newValue - r) / s
    return result


def normalizeValue(value, minValue, maxValue):
    return (value - minValue) / (maxValue - minValue)


def diagrammBTC():
    pygame.init()

    FONT = pygame.font.SysFont(None, SIZEFONT)

    fenetre = pygame.display.set_mode(WINDOWSIZE)
    pygame.display.set_caption("cours du bitcoin")

    time_per_frame = 20 / 60
    last_time = time.time()
    frameCount = 0

    run = True
    listePrice = []
    start = 0

    start = float(getPriceBTCToEUR())

    while run:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            for event in pygame.event.get():
                if event.type == QUIT:
                    run = False

            keys = pygame.key.get_pressed()
            if keys[pygame.K_ESCAPE]:
                run = False

        fenetre.fill((0, 0, 0))

        prixBTC = getPriceBTCToEUR()

        btcMin = float(prixBTC) - DISTANCE / 2
        btcMax = float(prixBTC) + DISTANCE / 2

        if len(listePrice) == NBR:
            listePrice.pop(0)
            listePrice.append(prixBTC)
        else:
            listePrice.append(prixBTC)

        for i in range(0, len(listePrice)):
            x = i * (WINDOWSIZE[0] / NBR)
            y = mapValue(float(listePrice[i]), btcMin, btcMax, 0, 500)
            y = WINDOWSIZE[1] - y

            pygame.draw.circle(fenetre, (255, 255, 255), (x, y), 1, 1)

            if i != 0:
                lineX = (i - 1) * (WINDOWSIZE[0] / NBR)
                lineY = mapValue(float(listePrice[i - 1]), btcMin, btcMax, 0, 500)
                lineY = WINDOWSIZE[1] - lineY

                color = getColorBetweenPrice(listePrice[i - 1], listePrice[i])
                pygame.draw.line(fenetre, color, (x, y), (lineX, lineY))

        difference = float(listePrice[len(listePrice) - 1]) - float(listePrice[len(listePrice) - 2])
        color = getColorBetweenPrice(listePrice[len(listePrice) - 2], listePrice[len(listePrice) - 1])
        drawText(fenetre, FONT, str(round(difference, 2)) + "€", color, WINDOWSIZE[0] / 2, 40, True)
        drawText(fenetre, FONT, "session: " + str(round(float(prixBTC) - start, 2)) + "€", (255, 255, 255), 10, 10)
        drawText(fenetre, FONT, str(round(float(prixBTC), 2)) + "€", (255, 255, 255), WINDOWSIZE[0] / 2, 20, True)
        drawText(fenetre, FONT, "ESC: leave", (255, 255, 255), 5, WINDOWSIZE[1] - 20)

        pygame.display.update()

        last_time = time.time()
        frameCount += 1


start_new_thread(diagrammBTC, ())



#partie Bot discord
bot = commands.Bot(command_prefix=STARTWITH)

async def sendEmbedByAuthor(message, titre, text, url="", urlUser=""):
    embed = discord.Embed(title=titre, url=url, description=text, color=discord.Color.blue())
    embed.set_author(name=message.author.display_name, url=urlUser, icon_url=message.author.avatar_url)
    await message.channel.send(embed=embed)

async def sendEmbedMessageBot(idChannel, titre, text, url="", urlUser=""):
    channel = bot.get_channel(idChannel)
    embed = discord.Embed(title=titre, url=url, description=text, color=discord.Color.gold())
    embed.set_author(name=bot.user.name, url=urlUser, icon_url=bot.user.avatar_url)
    await channel.send(embed=embed)

async def sendEmbedImageByAuthor(message, fileName):
    picture = discord.File(fileName)
    embed = discord.Embed()
    embed.set_author(name=bot.user.name, icon_url=bot.user.avatar_url)
    embed.set_image(url="attachment://" + fileName)
    await message.channel.send(file=picture, embed=embed)

def getCryptoPriceToMonaie(money, conversion="eur"):
    session = Session()
    reponse = session.get(URL + money.upper() + conversion.upper())
    data = json.loads(reponse.text)

    res = None
    for key in data:
        if key == "symbol" or key == "price":
            res = data["price"]
    return res


def createImage(crypto, monnaie, file):
    listePrice = []

    time_per_frame = 20 / 60
    last_time = time.time()
    frameCount = 0

    while len(listePrice) < 50:
        delta_time = time.time() - last_time
        while delta_time < time_per_frame:
            delta_time = time.time() - last_time

            listePrice.append(getCryptoPriceToMonaie(crypto, monnaie))

        last_time = time.time()
        frameCount += 1

    listePriceFloat = []
    for i in range(0, len(listePrice)):
        listePriceFloat.append(float(listePrice[i]))

    cryptoMin = min(listePriceFloat)
    cryptoMax = max(listePriceFloat)

    img = Image.new("RGB", WINDOWSIZE, (0, 0, 0))

    d = ImageDraw.Draw(img)

    for i in range(0, len(listePrice)):
        x = i * (WINDOWSIZE[0] / NBR)
        y = mapValue(float(listePrice[i]), cryptoMin, cryptoMax, 0, 500)
        y = WINDOWSIZE[1] - y

        d.ellipse((x, y, x + 1, y + 1), (255, 255, 255), None, 30)

        if i != 0:
            lineX = (i - 1) * (WINDOWSIZE[0] / NBR)
            lineY = mapValue(float(listePrice[i - 1]), cryptoMin, cryptoMax, 0, 500)
            lineY = WINDOWSIZE[1] - lineY

            color = getColorBetweenPrice(listePrice[i - 1], listePrice[i])
            d.line((x, y, lineX, lineY), color)

    d.text((5, 5), "Session: " + str(round(float(listePrice[len(listePrice) - 1]) - float(listePrice[0]), 2)) + " " + monnaie.upper())
    d.text((img.width / 2, 5), "prix: " + str(round(float(listePrice[len(listePrice) - 1]), 2)) + " " + monnaie.upper(), align="center")

    img.save(file)


async def diagrammImgDiscord(message, crypto, monnaie):
    if getCryptoPriceToMonaie(crypto, monnaie) != None:
        await message.channel.send("Veuiller attendre 30 secondes environ création en cours...")
        fileName = time.strftime("%S%M%H-%d%m%Y") + ".png"
        createImage(crypto, monnaie, fileName)
        await sendEmbedImageByAuthor(message, fileName)
        os.remove(fileName)
    else:
        await message.channel.send("Aucune cryptomonnaie de ce type.")


def discordBot():
    @bot.command()
    async def clear(message):
        if ADMINROLEID in [y.id for y in message.author.roles]:
            await message.channel.purge()

    @bot.command()
    async def crypto(message, arg1, arg2="eur"):
        prix = getCryptoPriceToMonaie(arg1, arg2)

        if prix != None:
            await sendEmbedByAuthor(message, arg1.upper() + " ---> " + arg2.upper(), str(round(float(prix), 2)) + " " + arg2.upper())
        else:
            await sendEmbedByAuthor(message, "Aucune relation trouvé", "l'api ne trouve pas de lien possible entre ces deux monnaies.", url="https://www.binance.com/fr")

    @bot.command()
    async def conversion(message, nombreCrypto, crypto, monnaie):
        prix = getCryptoPriceToMonaie(crypto, monnaie)

        if prix != None:
            await sendEmbedByAuthor(message, nombreCrypto + " " + crypto.upper() + " ---> " + monnaie.upper(), str(round(float(prix) * float(nombreCrypto), 2)) + " " + monnaie.upper())
        else:
            await sendEmbedByAuthor(message, "Aucune relation trouvé", "l'api ne trouve pas de lien possible entre ces deux monnaies.", url="https://www.binance.com/fr")

    @bot.command()
    async def courbe(message, arg1, arg2="eur"):
        await diagrammImgDiscord(message, arg1, arg2)


    @tasks.loop(hours=1)
    async def everyHours():
        await sendEmbedMessageBot(IDCHANNELHOURS, "BTC ---> EUR", str(round(float(getPriceBTCToEUR()), 2)) + "€")

    @tasks.loop(hours=24)
    async def everyDays():
        await sendEmbedMessageBot(IDCHANNELDAYS, "BTC ---> EUR", str(round(float(getPriceBTCToEUR()), 2)) + "€")

    @bot.event
    async def on_ready():
        everyHours.start()
        everyDays.start()

        mainChannel = bot.get_channel(IDMAINCHANNEL)
        await mainChannel.send("liste des commandes:")
        await mainChannel.send("!crypto <monnaie>")
        await mainChannel.send("!crypto <monnaie> <monnaie>")
        await mainChannel.send("!courbe <monnaie>")
        await mainChannel.send("!conversion <nombre> <monnaie> <monnaie>")

        logo = open("logo.jpg", 'rb').read()

        await bot.user.edit(avatar=logo)
        await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=ACTIVITE))

    bot.run(TOKEN)


start_new_thread(discordBot(), ())
